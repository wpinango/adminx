package com.example.wpinango.adminx.models;

import java.util.ArrayList;

/**
 * Created by wpinango on 9/19/17.
 */

public class Mode {

    private int game_id;
    private String mode;
    private String value;
    private int system;
    private int other;
    private int financier;
    private int producer;
    private int agency;
    private int bankmin;
    private int bankmax;
    private int potea;
    private int poteb;

    public int getGame_id() {
        return game_id;
    }

    public void setGame_id(int game_id) {
        this.game_id = game_id;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getSystem() {
        return system;
    }

    public void setSystem(int system) {
        this.system = system;
    }

    public int getOther() {
        return other;
    }

    public void setOther(int other) {
        this.other = other;
    }

    public int getFinancier() {
        return financier;
    }

    public void setFinancier(int financier) {
        this.financier = financier;
    }

    public int getProducer() {
        return producer;
    }

    public void setProducer(int producer) {
        this.producer = producer;
    }

    public int getAgency() {
        return agency;
    }

    public void setAgency(int agency) {
        this.agency = agency;
    }

    public int getBankmin() {
        return bankmin;
    }

    public void setBankmin(int bankmin) {
        this.bankmin = bankmin;
    }

    public int getBankmax() {
        return bankmax;
    }

    public void setBankmax(int bankmax) {
        this.bankmax = bankmax;
    }

    public int getPotea() {
        return potea;
    }

    public void setPotea(int potea) {
        this.potea = potea;
    }

    public int getPoteb() {
        return poteb;
    }

    public void setPoteb(int poteb) {
        this.poteb = poteb;
    }

    public static Mode getSelectedMode(ArrayList<Mode> modes, int i){
        for (Mode m:modes) {
            if (m.getGame_id() == i && !modes.isEmpty()){
                return m;
            }
        }
        return null;
    }
}
