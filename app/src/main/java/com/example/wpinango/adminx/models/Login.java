package com.example.wpinango.adminx.models;

/**
 * Created by wpinango on 9/21/17.
 */

public class Login {
    private int id;
    private String token;
    private String error;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
