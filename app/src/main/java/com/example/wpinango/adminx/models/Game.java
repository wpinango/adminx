package com.example.wpinango.adminx.models;

import java.util.ArrayList;

/**
 * Created by wpinango on 9/19/17.
 */

public class Game {
    private String name;
    private int id;
    private boolean selected;

    public void setSelected(boolean selected){
        this.selected = selected;
    }

    public boolean isSelected(){
        return selected;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public static void deselectGames(ArrayList<Game>games){
        for (Game g : games){
            g.setSelected(false);
        }
    }

    public static int getGameIdSelected(ArrayList<Game>games){
        for (Game g : games){
            if (g.isSelected()){
                return g.getId();
            }
        }
        return 0;
    }

    public static boolean isGameSelected(ArrayList<Game>games){
        for (Game g : games){
            if (g.isSelected()){
                return true;
            }
        }
        return false;
    }
}
