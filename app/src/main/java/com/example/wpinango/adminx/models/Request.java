package com.example.wpinango.adminx.models;

/**
 * Created by wpinango on 9/21/17.
 */

public class Request {
    private String games;
    private String mode;
    private String fixn;
    private String fixp;
    private String system;
    private String other;
    private String financier;
    private String producer;
    private String agency;
    private String PoteA;
    private String PoteB;
    private String banMin;
    private String banMax;

    public String getGames() {
        return games;
    }

    public void setGames(String games) {
        this.games = games;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getFixn() {
        return fixn;
    }

    public void setFixn(String fixn) {
        this.fixn = fixn;
    }

    public String getFixp() {
        return fixp;
    }

    public void setFixp(String fixp) {
        this.fixp = fixp;
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getFinancier() {
        return financier;
    }

    public void setFinancier(String financier) {
        this.financier = financier;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getAgency() {
        return agency;
    }

    public void setAgency(String agency) {
        this.agency = agency;
    }

    public String getPoteA() {
        return PoteA;
    }

    public void setPoteA(String poteA) {
        this.PoteA = poteA;
    }

    public String getPoteB() {
        return PoteB;
    }

    public void setPoteB(String poteB) {
        this.PoteB = poteB;
    }

    public String getBanMin() {
        return banMin;
    }

    public void setBanMin(String banMin) {
        this.banMin = banMin;
    }

    public String getBanMax() {
        return banMax;
    }

    public void setBanMax(String banMax) {
        this.banMax = banMax;
    }
}
