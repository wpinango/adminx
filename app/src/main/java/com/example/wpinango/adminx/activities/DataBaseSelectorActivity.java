package com.example.wpinango.adminx.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.wpinango.adminx.Global;
import com.example.wpinango.adminx.R;
import com.example.wpinango.adminx.adapters.ExpandableListDBNameAdapter;
import com.example.wpinango.adminx.models.DBName;
import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by wpinango on 9/19/17.
 */

public class DataBaseSelectorActivity extends AppCompatActivity {
    private ArrayList<DBName> dbNames = new ArrayList<>();
    private ExpandableListDBNameAdapter expandableListDBNameAdapter;
    private ProgressBar progressBar;
    private Button btnNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_base_selector);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.INVISIBLE);
        Button btnRefresh = (Button) findViewById(R.id.btn_refresh_db_select);
        btnNext = (Button)findViewById(R.id.btn_next_db_select);
        btnNext.setEnabled(false);
        ExpandableListView expandableListView = (ExpandableListView) findViewById(R.id.ex_db_name);
        expandableListDBNameAdapter = new ExpandableListDBNameAdapter(this, dbNames);
        expandableListView.setAdapter(expandableListDBNameAdapter);
        new GetAllDataBase().execute(Global.URL_GET_ALL_DB + "/" +  String.valueOf(Global.id));
        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new GetAllDataBase().execute(Global.URL_GET_ALL_DB + "/" + String.valueOf(Global.id));
            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SendDBSelect().execute(Global.URL_GET_ALL_GAMES + DBName.getDBSelected(dbNames));
            }
        });
        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i1, long l) {
                for (int j = 0; j < dbNames.size(); j ++){
                    if (j == i1){
                        dbNames.get(i1).setSelected(!dbNames.get(i1).isSelected());
                    } else {
                        dbNames.get(j).setSelected(false);
                    }
                }
                expandableListDBNameAdapter.notifyDataSetChanged();
                if(DBName.isDBSelected(dbNames)){
                    btnNext.setEnabled(true);
                } else {
                    btnNext.setEnabled(false);
                }
                return true;
            }
        });
    }

    private class GetAllDataBase extends AsyncTask<String, String, String> {
        private Gson gson = new Gson();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            try {
                HttpRequest req = HttpRequest.get(currentUrl)//.accept("application/json")
                        .header(Global.KEY_AUTH,Global.KEY_TOKEN + Global.token)
                        .connectTimeout(5000)
                        .readTimeout(5000);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(DataBaseSelectorActivity.this, "Falla de red", Toast.LENGTH_SHORT);
            } catch (Exception e) {
                System.out.println("Error1 ex : " + e.getMessage());
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                dbNames.clear();
                btnNext.setEnabled(false);
                progressBar.setVisibility(View.INVISIBLE);
                if (!s.equals("")) {
                    DBName[] db = gson.fromJson(s,DBName[].class);
                    Collections.addAll(dbNames, db);
                    expandableListDBNameAdapter.notifyDataSetChanged();
                } else {
                    Global.Toaster.get().showToast(DataBaseSelectorActivity.this, "Algo salio mal", Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class SendDBSelect extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            try {
                HttpRequest req = HttpRequest.get(currentUrl)
                        .header(Global.KEY_AUTH,Global.KEY_TOKEN + Global.token)
                        .connectTimeout(5000)
                        .readTimeout(5000);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(DataBaseSelectorActivity.this, "Falla de red", Toast.LENGTH_SHORT);
            } catch (Exception e) {
                System.out.println("Error1 ex : " + e.getMessage());
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                progressBar.setVisibility(View.INVISIBLE);
                if (!s.equals("")) {
                    Intent intent = new Intent(DataBaseSelectorActivity.this,MainActivity.class);
                    intent.putExtra("peac",s);
                    startActivity(intent);
                    DataBaseSelectorActivity.this.finish();
                } else {
                    Global.Toaster.get().showToast(DataBaseSelectorActivity.this, "Algo salio mal", Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
