package com.example.wpinango.adminx.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.example.wpinango.adminx.R;
import com.example.wpinango.adminx.models.DBName;

import java.util.ArrayList;

/**
 * Created by wpinango on 9/20/17.
 */

public class ExpandableListDBNameAdapter extends BaseExpandableListAdapter {
    private LayoutInflater inflater;
    private Context context;
    private ArrayList<DBName> dbNames;

    public ExpandableListDBNameAdapter(Context context, ArrayList<DBName>dbNames ){
        this.context = context;
        this.dbNames = dbNames;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getGroupCount() {
        return 1;
    }

    @Override
    public int getChildrenCount(int i) {
        return dbNames.size();
    }

    @Override
    public Object getGroup(int i) {
        return null;
    }

    @Override
    public Object getChild(int i, int i1) {
        return dbNames.get(i1);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = inflater.inflate(R.layout.item_explandable_group, null);
        }
        TextView expandedListTextView = view.findViewById(R.id.tv_explandable_group);
        expandedListTextView.setText("Selecciona una base de datos");
        return view;
    }

    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {

        if (view == null) {
            view = inflater.inflate(R.layout.item_expandable_child, null);
        }
        TextView expandedListTextView = view.findViewById(R.id.tv_expandable_child);
        expandedListTextView.setText(dbNames.get(i1).getDb());
        if (dbNames.get(i1).isSelected()){
            view.setBackgroundColor(Color.LTGRAY);
        } else {
            view.setBackgroundColor(Color.WHITE);
        }
        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }
}
