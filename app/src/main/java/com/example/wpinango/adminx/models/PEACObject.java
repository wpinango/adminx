package com.example.wpinango.adminx.models;

import java.util.ArrayList;

/**
 * Created by wpinango on 9/19/17.
 */

public class PEACObject {
    private ArrayList<Game> games;
    private ArrayList<Mode> modes;

    public ArrayList<Game> getGames() {
        return games;
    }

    public void setGames(ArrayList<Game> games) {
        this.games = games;
    }

    public ArrayList<Mode> getModes() {
        return modes;
    }

    public void setModes(ArrayList<Mode> modes) {
        this.modes = modes;
    }
}
