package com.example.wpinango.adminx.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.wpinango.adminx.Global;
import com.example.wpinango.adminx.R;
import com.example.wpinango.adminx.models.Login;
import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;


/**
 * Created by wpinango on 9/21/17.
 */

public class LoginActivity extends AppCompatActivity {
    private EditText userLogin, passwordText;
    private ProgressBar progressBar;
    private Button loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        progressBar = (ProgressBar) findViewById(R.id.pb_login);
        progressBar.setVisibility(View.INVISIBLE);
        loginButton = (Button) findViewById(R.id.btn_login);
        userLogin = (EditText) findViewById(R.id.input_user);
        passwordText = (EditText) findViewById(R.id.input_pass);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validation()) {
                    loginButton.setEnabled(false);
                    new LoginAsyncTask().execute(userLogin.getText().toString().toLowerCase(),passwordText.getText().toString());
                }
            }
        });
    }

    private boolean validation() {
        boolean valid = true;
        String userName = userLogin.getText().toString();
        String pass = passwordText.getText().toString();
        if (userName.isEmpty()) {
            valid = false;
            userLogin.setError("Debe introducir un nombre de usuario");
        } else {
            userLogin.setError(null);
        }
        if (pass.isEmpty()) {
            passwordText.setError("Debe introducir una contraseña");
        } else {
            passwordText.setError(null);
        }
        return valid;
    }

    private class LoginAsyncTask extends AsyncTask<String, Integer, String> {
        private HttpRequest request;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            String user_name = params[0];
            String password = params[1];
            try {
                request = HttpRequest.post(Global.URL_LOGIN)
                        .accept("application/json")
                        .basic(user_name, password)
                        .connectTimeout(5000)
                        .readTimeout(5000);
                return request.body();
            } catch (HttpRequest.HttpRequestException ex) {
                Global.Toaster.get().showToast(LoginActivity.this, ex.getMessage(), Toast.LENGTH_SHORT);
                return "";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            loginButton.setEnabled(true);
            progressBar.setVisibility(View.GONE);
            try {
                System.out.println("acceso :" + s);
                if (!s.isEmpty()) {
                    Login login = new Gson().fromJson(s,Login.class);
                    if (login.getError().equals("")) {
                        Global.token = login.getToken();
                        Global.id = login.getId();
                        Intent intent = new Intent(LoginActivity.this, DataBaseSelectorActivity.class);
                        startActivity(intent);
                        LoginActivity.this.finish();
                        saveIdentity(Global.id,Global.token);
                    } else {
                        Global.Toaster.get().showToast(LoginActivity.this, "Algo no esta bien", Toast.LENGTH_SHORT);
                    }
                } else {
                    Global.Toaster.get().showToast(LoginActivity.this, "Algo salio mal", Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void saveIdentity(int id, String token){
        //SharedPreferences sharedPreferences = getSharedPreferences().registerOnSharedPreferenceChangeListener();
    } //TODO save identity in sharedPreferences
}
