package com.example.wpinango.adminx.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TableLayout;
import android.widget.TextView;

import com.example.wpinango.adminx.Global;
import com.example.wpinango.adminx.R;
import com.example.wpinango.adminx.activities.DataBaseSelectorActivity;
import com.example.wpinango.adminx.activities.MainActivity;
import com.example.wpinango.adminx.adapters.ExpandableListGamesAdapter;
import com.example.wpinango.adminx.models.Game;
import com.example.wpinango.adminx.models.Mode;
import com.example.wpinango.adminx.models.PEACObject;
import com.google.gson.Gson;

/**
 * Created by wpinango on 9/19/17.
 */

public class InformationGameFragment extends Fragment {
    private PEACObject peacObject;
    private ExpandableListGamesAdapter expandableListAdapter;
    private TextView tvSystem, tvFinacier, tvValue, tvOther, tvProducer, tvAgency, tvBankMin,
            tvBankMax, tvPoteA, tvPoteB, tvMode;
    private TableLayout tableLayout;
    private TextView textView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_game_information, container, false);
        ExpandableListView expandableListView = rootView.findViewById(R.id.lv_games_modes);
        expandableListAdapter = new ExpandableListGamesAdapter(getActivity(), peacObject);
        expandableListView.setAdapter(expandableListAdapter);
        Button btnNext = rootView.findViewById(R.id.next_button_info);
        Button btnBack = rootView.findViewById(R.id.prev_button_info);
        tableLayout = rootView.findViewById(R.id.tb_layout);
        tvFinacier = rootView.findViewById(R.id.tv_financier);
        tvProducer = rootView.findViewById(R.id.tv_producer);
        tvBankMax = rootView.findViewById(R.id.tv_bank_max);
        tvBankMin = rootView.findViewById(R.id.tv_bank_min);
        tvAgency = rootView.findViewById(R.id.tv_agency);
        textView = rootView.findViewById(R.id.textView9);
        tvSystem = rootView.findViewById(R.id.tv_system);
        tvValue = rootView.findViewById(R.id.tv_value);
        tvOther = rootView.findViewById(R.id.tv_other);
        tvPoteA = rootView.findViewById(R.id.tv_pote_a);
        tvPoteB = rootView.findViewById(R.id.tv_pote_b);
        tvMode = rootView.findViewById(R.id.tv_mode);
        tableLayout.setVisibility(View.INVISIBLE);
        textView.setVisibility(View.INVISIBLE);
        btnNext.setEnabled(false);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).setSecondFragment();
            }
        });
        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i1, long l) {
                Game.deselectGames(peacObject.getGames());
                peacObject.getGames().get(i1).setSelected(!peacObject.getGames().get(i1).isSelected());
                expandableListAdapter.notifyDataSetChanged();
                if (Game.isGameSelected(peacObject.getGames()) && !peacObject.getModes().isEmpty()) {
                    setCurrentGameSelected(Mode.getSelectedMode(peacObject.getModes(), Game.getGameIdSelected(peacObject.getGames())));
                    tableLayout.setVisibility(View.VISIBLE);
                    btnNext.setEnabled(true);
                    textView.setVisibility(View.INVISIBLE);
                } else if (peacObject.getModes().isEmpty()) {
                    textView.setVisibility(View.VISIBLE);
                    btnNext.setEnabled(true);
                } else {
                    tableLayout.setVisibility(View.INVISIBLE);
                    btnNext.setEnabled(false);
                }
                return true;
            }
        });
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), DataBaseSelectorActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
        return rootView;
    }

    private void setCurrentGameSelected(Mode modeSelected) {
        tvProducer.setText(String.valueOf(modeSelected.getProducer()));
        tvFinacier.setText(String.valueOf(modeSelected.getFinancier()));
        tvBankMax.setText(String.valueOf(modeSelected.getBankmax()));
        tvBankMin.setText(String.valueOf(modeSelected.getBankmin()));
        tvAgency.setText(String.valueOf(modeSelected.getAgency()));
        tvSystem.setText(String.valueOf(modeSelected.getSystem()));
        tvPoteA.setText(String.valueOf(modeSelected.getPotea()));
        tvPoteB.setText(String.valueOf(modeSelected.getPoteb()));
        tvValue.setText(modeSelected.getValue());
        tvOther.setText(String.valueOf(modeSelected.getOther()));
        tvMode.setText(setMode(modeSelected.getMode()).toUpperCase());
        Global.modeSelected = modeSelected;
    }

    private String setMode(String mode){
        switch (mode){
            case "rndn":
                return "RND#";
            case "rndp":
                return "RND%";
            case "fixn":
                return "FIX#";
            case "fixp":
                return "FIX%";
            default:
                return mode;
        }
    }

    public void setDBInformation(String value) {
        peacObject = new Gson().fromJson(value, PEACObject.class);
    }
}
