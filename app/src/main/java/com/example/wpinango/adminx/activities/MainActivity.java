package com.example.wpinango.adminx.activities;

import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.wpinango.adminx.R;
import com.example.wpinango.adminx.adapters.CustomViewPager;
import com.example.wpinango.adminx.adapters.SectionsPagerAdapter;

import com.kofigyan.stateprogressbar.StateProgressBar;

public class MainActivity extends AppCompatActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private String[] descriptionData = {"Juego", "Modo", "Porcentaje"};
    private CustomViewPager mViewPager;
    private StateProgressBar stateProgressBar;
    private android.support.v7.app.ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/
        actionBar = getSupportActionBar();
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = (CustomViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setPagingEnabled(false);
        stateProgressBar = (StateProgressBar) findViewById(R.id.stateProgressBar);
        stateProgressBar.setStateDescriptionData(descriptionData);
        String peacString = getIntent().getStringExtra("peac");
        if (peacString != ""){
            //PEACObject peacObjtect = new Gson().fromJson(peacString,PEACObject.class);
            setDBInformation(peacString);
        }
        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
*/
    }

    private void setDBInformation(String value){
        mSectionsPagerAdapter.setDBInformation(value);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public enum StateNumber {
        ONE(1), TWO(2), THREE(3), FOUR(4), FIVE(5);
        private int value;

        StateNumber(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    public void setFirstFragment(){
        mViewPager.setCurrentItem(0);
        stateProgressBar.setCurrentStateNumber(StateProgressBar.StateNumber.ONE);
    }

    public void setSecondFragment(){
        mViewPager.setCurrentItem(1);
        mSectionsPagerAdapter.populateRB();
        stateProgressBar.setCurrentStateNumber(StateProgressBar.StateNumber.TWO);
    }

    public void setThirdFragment(){
        mViewPager.setCurrentItem(2);
        mSectionsPagerAdapter.populateList();
        stateProgressBar.setCurrentStateNumber(StateProgressBar.StateNumber.THREE);
    }

    public void setToolbarTitle(String title){
        actionBar.setSubtitle(title);
    }

}
