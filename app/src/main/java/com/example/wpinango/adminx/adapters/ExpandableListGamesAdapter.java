package com.example.wpinango.adminx.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.example.wpinango.adminx.R;
import com.example.wpinango.adminx.models.PEACObject;

/**
 * Created by wpinango on 9/20/17.
 */

public class ExpandableListGamesAdapter extends BaseExpandableListAdapter {
    private Context context;
    private PEACObject peacObject;
    private LayoutInflater inflater;

    public ExpandableListGamesAdapter(Context context, PEACObject peacObject){
        this.context = context;
        this.peacObject = peacObject;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getGroupCount() {
        return 1;
    }

    @Override
    public int getChildrenCount(int i) {
        return peacObject.getGames().size();
    }

    @Override
    public Object getGroup(int i) {
        return null;
    }

    @Override
    public Object getChild(int i, int i1) {
        return peacObject.getGames().get(i);
    }

    @Override
    public long getGroupId(int i) {
        return 0;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = inflater.inflate(R.layout.item_explandable_group, null);
        }
        TextView expandedListTextView = view.findViewById(R.id.tv_explandable_group);
        expandedListTextView.setText("Selecciona un juego");
        return view;
    }

    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {

        if (view == null) {
            view = inflater.inflate(R.layout.item_expandable_child, null);
        }
        TextView expandedListTextView = view.findViewById(R.id.tv_expandable_child);
        expandedListTextView.setText(peacObject.getGames().get(i1).getName().toUpperCase());
        if (peacObject.getGames().get(i1).isSelected()){
            view.setBackgroundColor(Color.LTGRAY);
        } else {
            view.setBackgroundColor(Color.WHITE);
        }
        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }
}
