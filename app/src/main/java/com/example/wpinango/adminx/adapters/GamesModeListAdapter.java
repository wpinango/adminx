package com.example.wpinango.adminx.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.example.wpinango.adminx.R;
import com.example.wpinango.adminx.models.PEACObject;

public class GamesModeListAdapter extends BaseExpandableListAdapter {
    private PEACObject peacObject;
    private LayoutInflater inflater;

    public GamesModeListAdapter(Context context, PEACObject peacObject){
        this.peacObject = peacObject;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getGroupCount() {
        return peacObject.getGames().size();
    }

    @Override
    public int getChildrenCount(int i) {
        return peacObject.getModes().size();
    }

    @Override
    public Object getGroup(int i) {
        return peacObject.getGames().get(i);
    }

    @Override
    public Object getChild(int i, int i1) {
        return peacObject.getModes().get(i);
    }

    @Override
    public long getGroupId(int i) {
        return 0;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = inflater.inflate(R.layout.item_explandable_group, null);
        }
        TextView expandedListTextView = view.findViewById(R.id.tv_explandable_group);
        expandedListTextView.setText(peacObject.getGames().get(i).getName());
        return view;
    }

    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {

        if (view == null) {
            view = inflater.inflate(R.layout.item_list_mode, null);
        }

        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }
}
