package com.example.wpinango.adminx.models;

/**
 * Created by wpinango on 9/21/17.
 */

public class ModeList {
    private int value;
    private boolean checked;
    private String mode;

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
