package com.example.wpinango.adminx.fragments;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.wpinango.adminx.Global;
import com.example.wpinango.adminx.R;
import com.example.wpinango.adminx.activities.MainActivity;
import com.example.wpinango.adminx.models.Mode;

/**
 * Created by wpinango on 9/19/17.
 */

public class ModeFragment extends Fragment {
    private Button btnNext, btnBack;
    private RadioGroup radioGroup;
    private EditText etFixP, etFixN;
    private RadioButton rbOff,rbAuto,rbRndN, rbRndP,rbFixN,rbFixP,rbMin,rbMAx,rbMinTk,rbMaxTk,rbLost,rbFull;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_mode, container, false);
        radioGroup = rootView.findViewById(R.id.radiogroup);
        btnNext = rootView.findViewById(R.id.btn_next_mode);
        btnBack = rootView.findViewById(R.id.btn_back_mode);
        rbAuto = rootView.findViewById(R.id.rb_auto);
        rbOff = rootView.findViewById(R.id.rb_off);
        rbRndN = rootView.findViewById(R.id.rb_rnd_numeric);
        rbRndP = rootView.findViewById(R.id.rb_rnd_per);
        rbFixN = rootView.findViewById(R.id.rb_fix_numeric);
        rbFixP = rootView.findViewById(R.id.rb_fix_per);
        rbMin = rootView.findViewById(R.id.rb_min);
        rbMAx = rootView.findViewById(R.id.rb_max);
        rbMinTk = rootView.findViewById(R.id.rb_min_tk);
        rbMaxTk = rootView.findViewById(R.id.rb_max_tk);
        rbLost = rootView.findViewById(R.id.rb_lost);
        rbFull = rootView.findViewById(R.id.rb_full);
        etFixN = rootView.findViewById(R.id.et_fix_numeric);
        etFixP = rootView.findViewById(R.id.et_fix_per);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).setFirstFragment();
            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int selectedRadioButtonID = radioGroup.getCheckedRadioButtonId();
                if (selectedRadioButtonID != -1) {
                    RadioButton selectedRadioButton = rootView.findViewById(selectedRadioButtonID);
                    if (rbFixN.isChecked() || rbFixP.isChecked()){
                        if ((rbFixN.isChecked() && !etFixN.getText().toString().isEmpty()) || rbFixP.isChecked() &&
                                !etFixP.getText().toString().isEmpty()){
                            setModeSelected(selectedRadioButton.getText().toString().toLowerCase());
                            if (rbFixP.isChecked()) {
                                Global.modeSelected.setValue(etFixP.getText().toString());
                            } else if (rbFixN.isChecked()){
                                Global.modeSelected.setValue(etFixN.getText().toString());
                            }
                        } else {
                            Toast.makeText(getActivity(),"Debe introducir un valor valido",Toast.LENGTH_SHORT).show();
                        }
                    }  else{
                        setModeSelected(selectedRadioButton.getText().toString().toLowerCase());
                    }
                }
            }
        });
        rbFixN.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (rbFixN.isChecked()){
                    etFixN.setEnabled(true);
                } else {
                    etFixN.setEnabled(false);
                    Global.modeSelected.setValue("");
                }
            }
        });
        rbFixP.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (rbFixP.isChecked()){
                    etFixP.setEnabled(true);
                } else {
                    etFixP.setEnabled(false);
                    Global.modeSelected.setValue("");
                }
            }
        });
        return rootView;
    }

    private void setModeSelected(String mode){
        if (mode.contains("%")) {
            mode = mode.replace("%", "p");
        } else if (mode.contains("#")) {
            mode = mode.replace("#", "n");
        }
        Global.modeSelected.setMode(mode);
        ((MainActivity)getActivity()).setThirdFragment();
    }

    public void populateRB(Mode mode){ // off, auto, rndn, rndp, fixn, fixp, min, max, mintk, maxtk, lost, full
        radioGroup.clearCheck();
        switch (mode.getMode()){
            case "off":
                rbOff.setChecked(true);
                break;
            case "auto":
                rbAuto.setChecked(true);
                break;
            case "rndn":
                rbRndN.setChecked(true);
                break;
            case "rndp":
                rbRndP.setChecked(true);
                break;
            case "fixn":
                rbFixN.setChecked(true);
                etFixN.setText(mode.getValue());
                break;
            case "fixp":
                rbFixP.setChecked(true);
                etFixP.setText(mode.getValue());
                break;
            case "min":
                rbMin.setChecked(true);
                break;
            case "max":
                rbMAx.setChecked(true);
                break;
            case "mintk":
                rbMinTk.setChecked(true);
                break;
            case "maxtk":
                rbMaxTk.setChecked(true);
                break;
            case "lost":
                rbLost.setChecked(true);
                break;
            case "full":
                rbFull.setChecked(true);
                break;
        }
    }
}
