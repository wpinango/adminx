package com.example.wpinango.adminx.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.wpinango.adminx.Global;
import com.example.wpinango.adminx.R;
import com.example.wpinango.adminx.activities.DataBaseSelectorActivity;
import com.example.wpinango.adminx.activities.MainActivity;
import com.example.wpinango.adminx.models.Login;
import com.example.wpinango.adminx.models.Mode;
import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import cn.pedant.SweetAlert.SweetAlertDialog;


/**
 * Created by wpinango on 9/21/17.
 */

public class ValuesFragment extends Fragment {
    private Button btnBack, btnConfirm;
    private CheckBox cbSystem, cbOther, cbFinancier, cbProducer, cbAgency, cbPoteA, cbPoteB, cbBanca;
    private EditText etSystem, etOther, etFinancier, etProducer, etAgency, etPoteA, etPoteB, etMin, etMax;
    private ProgressBar progressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_values, container, false);
        init(rootView);
        btnBack.setOnClickListener(view -> ((MainActivity) getActivity()).setSecondFragment());
        btnConfirm.setOnClickListener(view -> {
            if (!etAgency.getText().toString().isEmpty() && !etFinancier.getText().toString().isEmpty() &&
                    !etMax.getText().toString().isEmpty() && !etMin.getText().toString().isEmpty() &&
                    !etOther.getText().toString().isEmpty() && !etProducer.getText().toString().isEmpty() &&
                    !etSystem.getText().toString().isEmpty() && !etPoteA.getText().toString().isEmpty() &&
                    !etPoteB.getText().toString().isEmpty()) {
                showDialog();
            } else {
                Toast.makeText(getActivity(),"Debe introducir un valor valido",Toast.LENGTH_SHORT).show();
            }
        });
        cbProducer.setOnCheckedChangeListener((compoundButton, b) -> {
            if (cbProducer.isChecked()) {
                etProducer.setEnabled(true);
            } else {
                etProducer.setEnabled(false);
            }
        });
        cbFinancier.setOnCheckedChangeListener((compoundButton, b) -> {
            if (cbFinancier.isChecked()) {
                etFinancier.setEnabled(true);
            } else {
                etFinancier.setEnabled(false);
            }
        });
        cbOther.setOnCheckedChangeListener((compoundButton, b) -> {
            if (cbOther.isChecked()) {
                etOther.setEnabled(true);
            } else {
                etOther.setEnabled(false);
            }
        });
        cbAgency.setOnCheckedChangeListener((compoundButton, b) -> {
            if (cbAgency.isChecked()) {
                etAgency.setEnabled(true);
            } else {
                etAgency.setEnabled(false);
            }
        });
        cbPoteA.setOnCheckedChangeListener((compoundButton, b) -> {
            if (cbPoteA.isChecked()) {
                etPoteA.setEnabled(true);
            } else {
                etPoteA.setEnabled(false);
            }
        });
        cbPoteB.setOnCheckedChangeListener((compoundButton, b) -> {
            if (cbPoteB.isChecked()) {
                etPoteB.setEnabled(true);
            } else {
                etPoteB.setEnabled(false);
            }
        });
        cbSystem.setOnCheckedChangeListener((compoundButton, b) -> {
            if (cbSystem.isChecked()) {
                etSystem.setEnabled(true);
            } else {
                etSystem.setEnabled(false);
            }
        });
        cbBanca.setOnCheckedChangeListener((compoundButton, b) -> {
            if (cbBanca.isChecked()) {
                etMin.setEnabled(true);
                etMax.setEnabled(true);
            } else {
                etMin.setEnabled(false);
                etMax.setEnabled(false);
            }
        });
        return rootView;
    }

    private void showDialog() {
        isItemsChecked();
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_confirm, null);
        TextView tvSystem, tvFinacier, tvValue, tvOther, tvProducer, tvAgency, tvBankMin,
                tvBankMax, tvPoteA, tvPoteB, tvMode;
        tvFinacier = view.findViewById(R.id.tv_financier_dialog);
        tvProducer = view.findViewById(R.id.tv_producer_dialog);
        tvBankMax = view.findViewById(R.id.tv_bank_max_dialog);
        tvBankMin = view.findViewById(R.id.tv_bank_min_dialog);
        tvAgency = view.findViewById(R.id.tv_agency_dialog);
        tvSystem = view.findViewById(R.id.tv_system_dialog);
        tvValue = view.findViewById(R.id.tv_value_dialog);
        tvOther = view.findViewById(R.id.tv_other_dialog);
        tvPoteA = view.findViewById(R.id.tv_pote_a_dialog);
        tvPoteB = view.findViewById(R.id.tv_pote_b_dialog);
        tvMode = view.findViewById(R.id.tv_mode_dialog);
        tvFinacier.setText(String.valueOf(Global.modeSelected.getFinancier()));
        tvAgency.setText(String.valueOf(Global.modeSelected.getAgency()));
        tvProducer.setText(String.valueOf(Global.modeSelected.getProducer()));
        tvBankMax.setText(String.valueOf(Global.modeSelected.getBankmax()));
        tvBankMin.setText(String.valueOf(Global.modeSelected.getBankmin()));
        tvSystem.setText(String.valueOf(Global.modeSelected.getSystem()));
        tvValue.setText(Global.modeSelected.getValue());
        tvOther.setText(String.valueOf(Global.modeSelected.getOther()));
        tvPoteA.setText(String.valueOf(Global.modeSelected.getPotea()));
        tvPoteB.setText(String.valueOf(Global.modeSelected.getPoteb()));
        tvMode.setText(Global.modeSelected.getMode());
        builder.setTitle("Confirme el modo del PEAC");
        builder.setCancelable(false);
        builder.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                btnConfirm.setEnabled(true);
                new SavePeacMode().execute(Global.URL_SAVE_PEAC);
            }
        });
        builder.setNegativeButton("Cerrar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                btnConfirm.setEnabled(true);
            }
        });
        builder.setView(view);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void isItemsChecked() {
        if (cbSystem.isChecked()) {
            Global.modeSelected.setSystem(Integer.valueOf(etSystem.getText().toString()));
        } else {
            Global.modeSelected.setSystem(0);
        }
        if (cbOther.isChecked()) {
            Global.modeSelected.setOther(Integer.valueOf(etOther.getText().toString()));
        } else {
            Global.modeSelected.setOther(0);
        }
        if (cbFinancier.isChecked()) {
            Global.modeSelected.setFinancier(Integer.valueOf(etFinancier.getText().toString()));
        } else {
            Global.modeSelected.setFinancier(0);
        }
        if (cbProducer.isChecked()) {
            Global.modeSelected.setProducer(Integer.valueOf(etProducer.getText().toString()));
        } else {
            Global.modeSelected.setProducer(0);
        }
        if (cbAgency.isChecked()) {
            Global.modeSelected.setAgency(Integer.valueOf(etAgency.getText().toString()));
        } else {
            Global.modeSelected.setAgency(0);
        }
        if (cbPoteA.isChecked()) {
            Global.modeSelected.setPotea(Integer.valueOf(etPoteA.getText().toString()));
        } else {
            Global.modeSelected.setPotea(0);
        }
        if (cbPoteB.isChecked()) {
            Global.modeSelected.setPoteb(Integer.valueOf(etPoteB.getText().toString()));
        } else {
            Global.modeSelected.setPoteb(0);
        }
        if (cbBanca.isChecked()) {
            Global.modeSelected.setBankmax(Integer.valueOf(etMax.getText().toString()));
            Global.modeSelected.setBankmin(Integer.valueOf(etMin.getText().toString()));
        } else {
            Global.modeSelected.setBankmax(0);
            Global.modeSelected.setBankmin(0);
        }
        if (cbSystem.isChecked()) {
            Global.modeSelected.setSystem(Integer.valueOf(etSystem.getText().toString()));
        } else {
            Global.modeSelected.setSystem(0);
        }
    }

    public void populateView(Mode mode) {
        etSystem.setText(String.valueOf(mode.getSystem()));
        etOther.setText(String.valueOf(mode.getOther()));
        etFinancier.setText(String.valueOf(mode.getProducer()));
        etProducer.setText(String.valueOf(mode.getProducer()));
        etAgency.setText(String.valueOf(mode.getAgency()));
        etPoteA.setText(String.valueOf(mode.getPotea()));
        etPoteB.setText(String.valueOf(mode.getPoteb()));
        etMin.setText(String.valueOf(mode.getBankmin()));
        etMax.setText(String.valueOf(mode.getBankmax()));
        checkItem(mode);
    }

    private void init(View rootView) {
        cbFinancier = rootView.findViewById(R.id.cb_financier);
        btnConfirm = rootView.findViewById(R.id.btn_confirm);
        btnBack = rootView.findViewById(R.id.btn_back_values);
        cbSystem = rootView.findViewById(R.id.cb_system);
        cbOther = rootView.findViewById(R.id.cb_other);
        cbProducer = rootView.findViewById(R.id.cb_producer);
        cbAgency = rootView.findViewById(R.id.cb_agency);
        cbPoteA = rootView.findViewById(R.id.cb_pote_a);
        cbPoteB = rootView.findViewById(R.id.cb_pote_b);
        cbBanca = rootView.findViewById(R.id.cb_bank);
        etSystem = rootView.findViewById(R.id.et_system);
        etOther = rootView.findViewById(R.id.et_other);
        etFinancier = rootView.findViewById(R.id.et_financier);
        etProducer = rootView.findViewById(R.id.et_producer);
        etAgency = rootView.findViewById(R.id.et_agency);
        etPoteA = rootView.findViewById(R.id.et_pote_a);
        etPoteB = rootView.findViewById(R.id.et_pote_b);
        etMin = rootView.findViewById(R.id.et_min);
        etMax = rootView.findViewById(R.id.et_max);
        progressBar = rootView.findViewById(R.id.progressBar2);
        progressBar.setVisibility(View.INVISIBLE);
    }

    private void checkItem(Mode mode) {
        if (mode.getSystem() != 0) {
            cbSystem.setChecked(true);
            etSystem.setEnabled(true);
        }
        if (mode.getOther() != 0) {
            cbOther.setChecked(true);
            etOther.setEnabled(true);
        }
        if (mode.getFinancier() != 0) {
            cbFinancier.setChecked(true);
            etFinancier.setEnabled(true);
        }
        if (mode.getProducer() != 0) {
            cbProducer.setChecked(true);
            etProducer.setEnabled(true);
        }
        if (mode.getAgency() != 0) {
            cbAgency.setChecked(true);
            etAgency.setEnabled(true);
        }
        if (mode.getPotea() != 0) {
            cbPoteA.setChecked(true);
            etPoteA.setEnabled(true);
        }
        if (mode.getPoteb() != 0) {
            cbPoteB.setChecked(true);
            etPoteB.setEnabled(true);
        }
        if (mode.getBankmax() != 0 || mode.getBankmin() != 0) {
            cbBanca.setChecked(true);
            etMax.setEnabled(true);
            etMin.setEnabled(true);
        }
        if (mode.getSystem() != 0) {
            cbSystem.setChecked(true);
            etSystem.setEnabled(true);
        }
    }

    private class SavePeacMode extends AsyncTask<String, Integer, String> {
        private HttpRequest request;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            String url = params[0];
            try {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("games", Global.modeSelected.getGame_id());
                jsonObject.addProperty("mode", Global.modeSelected.getMode());
                jsonObject.addProperty("value", Global.modeSelected.getValue());
                jsonObject.addProperty("system", Global.modeSelected.getSystem());
                jsonObject.addProperty("other", Global.modeSelected.getOther());
                jsonObject.addProperty("financier", Global.modeSelected.getFinancier());
                jsonObject.addProperty("producer", Global.modeSelected.getProducer());
                jsonObject.addProperty("agency", Global.modeSelected.getAgency());
                jsonObject.addProperty("bankMax", Global.modeSelected.getBankmax());
                jsonObject.addProperty("bankMin", Global.modeSelected.getBankmin());
                jsonObject.addProperty("PoteA", Global.modeSelected.getPotea());
                jsonObject.addProperty("Poteb", Global.modeSelected.getPoteb());
                request = HttpRequest.post(url)
                        .accept("application/json")
                        .header(Global.KEY_AUTH, Global.KEY_TOKEN + Global.token)
                        .send(jsonObject.toString())
                        .connectTimeout(5000)
                        .readTimeout(5000);
                return request.body();
            } catch (HttpRequest.HttpRequestException ex) {
                Global.Toaster.get().showToast(getActivity(), ex.getMessage(), Toast.LENGTH_SHORT);
                return "";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            progressBar.setVisibility(View.GONE);
            Log.d("valores ", s);
            try {
                if (!s.isEmpty()) {
                    Login login = new Gson().fromJson(s, Login.class);
                    if (login.getError().equals("")) {
                        new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Datos correctos")
                                .setContentText("Los datos del peac fueron cambiados exitosamente")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        Intent intent = new Intent(getActivity(), DataBaseSelectorActivity.class);
                                        startActivity(intent);
                                        getActivity().finish();
                                    }
                                })
                                .show();
                    } else {
                        Global.Toaster.get().showToast(getActivity(), "Algo no esta bien", Toast.LENGTH_SHORT);
                    }
                } else {
                    Global.Toaster.get().showToast(getActivity(), "Algo salio mal", Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
