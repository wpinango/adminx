package com.example.wpinango.adminx.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.example.wpinango.adminx.Global;
import com.example.wpinango.adminx.fragments.InformationGameFragment;
import com.example.wpinango.adminx.fragments.ModeFragment;
import com.example.wpinango.adminx.fragments.ValuesFragment;

/**
 * Created by wpinango on 9/19/17.
 */

public class SectionsPagerAdapter extends FragmentPagerAdapter {
    private InformationGameFragment primaryFragment = new InformationGameFragment();
    private ModeFragment secondaryFragment = new ModeFragment();
    private ValuesFragment valuesFragment = new ValuesFragment();
    private Fragment mPrimaryItem;

    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return primaryFragment;
            case 1:
                return secondaryFragment;
            case 2:
                return valuesFragment;
        }
        return null;
    }

    @Override
    public int getItemPosition(Object object) {
        // TODO: be smarter about this
        if (object == mPrimaryItem) {
            // Re-use the current fragment (its position never changes)
            return POSITION_UNCHANGED;
        }
        return POSITION_NONE;
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        super.setPrimaryItem(container, position, object);
        mPrimaryItem = (Fragment) object;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "1";
            case 1:
                return "2";
            case 3:
                return "3";
        }
        return null;
    }

    public void populateRB(){
        secondaryFragment.populateRB(Global.modeSelected);
    }

    public void populateList(){
        valuesFragment.populateView(Global.modeSelected);
    }

    public void setDBInformation(String value) {
        primaryFragment.setDBInformation(value);
    }
}