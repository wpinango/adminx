package com.example.wpinango.adminx.models;

import java.util.ArrayList;

/**
 * Created by wpinango on 9/19/17.
 */

public class DBName {
    private String db;
    private boolean selected;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getDb() {
        return db;
    }

    public void setDb(String db) {
        this.db = db;
    }

    public static boolean isDBSelected(ArrayList<DBName>dbNames){
        for (DBName db: dbNames) {
            if(db.isSelected()){
                return true;
            }
        }
        return false;
    }

    public static String getDBSelected(ArrayList<DBName>dbNames){
        for (DBName db: dbNames) {
            if(db.isSelected()){
                return db.getDb();
            }
        }
        return "";
    }

}
