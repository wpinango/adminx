package com.example.wpinango.adminx;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import com.example.wpinango.adminx.models.Mode;


/**
 * Created by wpinango on 9/20/17.
 */

public class Global {
    private static String HOST = "http://lotosuite.com:80";
    //private static String HOST = "http://172.16.10.175/sgt/public/index.php";
    //private static String HOST = "http://172.16.29.120/lot-up/public/index.php";
    public static String URL_GET_ALL_DB = HOST + "/getDBs";
    public static String URL_GET_ALL_GAMES = HOST  + "/getSettings/";
    public static String URL_LOGIN = HOST + "/postLogin";
    public static String URL_SAVE_PEAC = HOST + "/postSaveModes ";
    public static Mode modeSelected;
    public static String token;
    public static String KEY_TOKEN = "bearer ";
    public static String KEY_AUTH = "Authorization";
    public static int id;

    public enum Toaster {
        INSTANCE;
        private final Handler handler = new Handler(Looper.getMainLooper());

        public void showToast(final Context context, final String message, final int length) {
            if (context != null) {
                handler.post(
                        () -> Toast.makeText(context, message, length).show()
                );
            }
        }

        public static Toaster get() {
            return INSTANCE;
        }
    }


}
